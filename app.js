// const ngrok = require('ngrok')

const express = require("express");

const cors = require("cors");

const app = express();

const server = require("http").createServer(app);

const io = require("socket.io")(server);

const PORT = 5000;

const router = require("./routes/router");

const { addUser, getUser, getUsersInRoom, removeUser } = require("./controller/users");

app.use(cors);

app.use(router);

io.on("connection", (socket) => {

  console.log("[NEW SOCKET]", socket.id);

  socket.emit('addSocket', true);

  socket.on("join", (data,callback) => {

    console.log("[JOIN]", data);

    const { name, room } = data;

    const { error, user } = addUser({ id: socket.id, name, room });

    if (error) return;

    console.log("[user]", user);

    socket.join(user.room);

    console.log("[socket Rooms]", socket.rooms, Object.keys(socket.rooms));

    socket.emit("message", {
      user: "admin",
      text: `${user.name}, Welcome to the room ${user.room}`,
      _id: socket.id
    });

    // socket.broadcast
    //   .to(user.room)
    //   .emit("message", { user: "admin", text: `${user.name} has joined!` });

    socket.broadcast.to(user.room).emit("message", {
      user: "admin",
      text: `${user.name} has joined!`
    });

  });
  

  socket.on("sendMessage", (message,callback) => {

    console.log("[mesasge]", message)

    const user = getUser(socket.id);

    // socket.broadcast.to(user.room).emit("message", { user: user.name, text: message.text, _id: socket.id });

    io.to(message.room).emit("message", { user:user && user.name || "sa", text: message.text, _id: socket.id });
    callback();
  });

  socket.on("disconnect", () => {
    const user = removeUser(socket.id);

    if (user) {
      console.log("[DISCONNECTED USER]", user, socket.rooms);

      socket.leave(user.room);

      io.to(user.room).emit("message", {
        user: "admin",
        text: `${user.name} has left the room!`,
      });
    } else {
      console.log("[DISCONNECTED USER]", socket.id, socket.rooms);

      socket.leave(socket.id);
    }

  });

});

app.use(cors);

server.listen(PORT, () => console.log(`Server is running on port: ${PORT}`));

// ngrok.connect(PORT).then((url) => {
//   console.log(`Server forwarded to public url ${url}`);
// });


